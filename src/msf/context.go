package msf

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
)

type Context struct {
	path    string
	m       map[string]any
	addrMap map[string]string

	r *http.Request
	w *http.ResponseWriter
}

//
// MAP
//

func (ctx *Context) Get(key string) (val any, exists bool) {
	val, exists = ctx.m[key]
	return
}

func (ctx *Context) GetString(key string) (s string) {
	if val, exists := ctx.Get(key); val != nil && exists {
		s = val.(string)
	}
	return
}

func (ctx *Context) Add(key string, val any) {
	ctx.m[key] = val
}

func (ctx *Context) AddString(key string, val string) {
	ctx.m[key] = val
}

//
// REQUEST
//

func (ctx *Context) RequestBytes(msName string, path string) ([]byte, error) {
	return ctx.request(msName, path)
}

func (ctx *Context) RequestJSON(msName string, path string) (map[string]any, error) {
	var out map[string]any

	b, err := ctx.request(msName, path)
	if err != nil {
		return out, err
	}

	err = json.Unmarshal(b, &out)
	if err != nil {
		return out, err
	}

	return out, nil
}

func (ctx *Context) RequestList(msName string, path string) ([]any, error) {
	var out []any

	b, err := ctx.request(msName, path)
	if err != nil {
		return out, err
	}

	err = json.Unmarshal(b, &out)
	if err != nil {
		return out, err
	}

	return out, nil
}

func (ctx *Context) request(msName string, path string) ([]byte, error) {
	in, err := json.Marshal(ctx.m)
	if err != nil {
		return nil, err
	}

	reader := bytes.NewReader(in)
	response, err := http.Post(ctx.addrMap[msName]+path, "application/json", reader)
	if err != nil {
		return nil, err
	}

	b, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return b, nil
}

//
// RESPONSE
//

func (ctx *Context) Response(code int) {
	ctx.response(code, []byte("{}"), nil)
}

func (ctx *Context) ResponseError(code int, err error) {
	ctx.response(code, []byte("{}"), err)
}

func (ctx *Context) ResponseBytes(code int, b []byte) {
	ctx.response(code, b, nil)
}

func (ctx *Context) ResponseJSON(code int, obj any) {
	b, err := json.Marshal(obj)
	if err != nil {
		ctx.response(http.StatusInternalServerError, []byte("{}"), err)
		return
	}
	ctx.response(code, b, nil)
}

func (ctx *Context) response(code int, data []byte, err error) {
	writer := *ctx.w
	writer.WriteHeader(code)
	_, _ = writer.Write(data)
	if err != nil {
		log.Default().Println(ctx.path, code, err)
	} else {
		log.Default().Println(ctx.path, code)
	}
}
