package msf

import (
	"database/sql"
	"encoding/json"
	"io"
	"log"
	"net/http"
)

type Microservice struct {
	mux     *http.ServeMux
	addrMap map[string]string
	db      *sql.DB
}

func New() *Microservice {
	return &Microservice{mux: http.NewServeMux()}
}

//
// INITIALIZATION
//

// InitDB initializes database connection using given driver name and data source
// name and returns sql.DB object.
//
// If sql.Open error or ping error occurs, log.Fatal method will be called
// which means application will crash.
func (ms *Microservice) InitDB(driverName string, dataSourceName string) *sql.DB {
	db, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		log.Fatal(err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	log.Default().Print("Initialized database...")
	return db
}

// InitAddrMap initializes address map of microservice.
func (ms *Microservice) InitAddrMap(addrMap map[string]string) {
	ms.addrMap = addrMap
}

// POST adds an endpoint to microservice which can be accessed by the path
// mentioned and the function passed as an argument will be executed.
func (ms *Microservice) POST(path string, fn func(*Context)) {
	ms.mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		ms.handle(&w, r, path, fn)
	})
}

// GWPOST registers an endpoint which redirects a request to some endpoint in
// another microservice
func (ms *Microservice) GWPOST(msName string, path string) {
	fullPath := "/" + msName + path
	ms.POST(fullPath, func(ctx *Context) {
		out, err := ctx.RequestBytes(msName, path)
		if err != nil {
			ctx.ResponseError(http.StatusInternalServerError, err)
			return
		}
		ctx.ResponseBytes(http.StatusOK, out)
	})
}

// Start starts a server on given address to serve endpoints defined in microservice.
func (ms *Microservice) Start(addr string) {
	log.Default().Printf("Listening to %s", addr)
	err := http.ListenAndServe(addr, ms.mux)
	if err != nil {
		log.Fatal(err)
	}
}

//
//
//

// handle receives request from a client and calls a function passed as an argument on
// its context object.
// It first reads request body and unmarshalls it into context's map.
// If there is any error in the process of reading or unmarshalling, log message is printed and function is not called.
//
// Then it creates a new context object with received parameters and calls the function passed as argument on it.
func (ms *Microservice) handle(w *http.ResponseWriter, r *http.Request, path string, fn func(*Context)) {
	b, err := io.ReadAll(r.Body)
	if err != nil {
		log.Default().Print(err)
		return
	}

	var m map[string]any
	err = json.Unmarshal(b, &m)
	if err != nil {
		log.Default().Print(err)
		return
	}

	ctx := Context{
		path:    path,
		m:       m,
		addrMap: ms.addrMap,
		w:       w,
		r:       r,
	}
	fn(&ctx)
}
