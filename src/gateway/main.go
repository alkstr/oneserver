package main

import (
	"msf"
)

var addrMap = map[string]string{
	"gateway": "http://localhost:8100",
	"auth":    "http://localhost:8120",
	"chat":    "http://localhost:8121",
}

func main() {
	ms := msf.New()

	ms.InitAddrMap(addrMap)

	ms.GWPOST("auth", "/get-user")
	ms.GWPOST("auth", "/get-token")
	ms.GWPOST("auth", "/create-user")
	ms.GWPOST("auth", "/get-online")

	ms.GWPOST("chat", "/get-messages")
	ms.GWPOST("chat", "/send-message")

	ms.GWPOST("chat", "/create-channel")
	ms.GWPOST("chat", "/remove-channel")
	ms.GWPOST("chat", "/get-channels")

	ms.Start(":8100")
}
