package main

import (
	"chat/api"
	"chat/services"
	_ "modernc.org/sqlite"
	"msf"
)

var addrMap = map[string]string{
	"gateway": "http://localhost:8100",
	"auth":    "http://localhost:8120",
	"chat":    "http://localhost:8121",
}

func main() {
	ms := msf.New()
	services.DB = ms.InitDB("sqlite", "chat.sqlite")
	defer services.DB.Close()

	ms.InitAddrMap(addrMap)

	ms.POST("/send-message", api.SendMessage)
	ms.POST("/get-messages", api.GetMessages)

	ms.POST("/create-channel", api.CreateChannel)
	ms.POST("/remove-channel", api.RemoveChannel)
	ms.POST("/get-channels", api.GetChannels)

	ms.Start(":8121")
}
