package api

import (
	"chat/services"
	"database/sql"
	"fmt"
	"msf"
	"net/http"
	"time"
)

const (
	getMessagesFmt = `SELECT user, text, time FROM "%s"`
	sendMessageFmt = `INSERT INTO "%s" (user, text, time) VALUES (?, ?, ?)`
)

type message struct {
	User string
	Text string
	Time int64
}

func GetMessages(ctx *msf.Context) {
	ch := ctx.GetString("Channel")

	getMessagesStmt := fmt.Sprintf(getMessagesFmt, ch)

	stmt, err := services.DB.Prepare(getMessagesStmt)
	defer stmt.Close()
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	var rows *sql.Rows
	rows, err = services.DB.Query(getMessagesStmt)
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	var messages []message
	for rows.Next() {
		m := message{}
		_ = rows.Scan(&m.User, &m.Text, &m.Time)
		messages = append(messages, m)
	}

	ctx.ResponseJSON(http.StatusOK, messages)
}

func SendMessage(ctx *msf.Context) {
	ch := ctx.GetString("Channel")
	text := ctx.GetString("Text")

	user, err := ctx.RequestJSON("auth", "/get-user")
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	sendMessageStmt := fmt.Sprintf(sendMessageFmt, ch)

	stmt, err := services.DB.Prepare(sendMessageStmt)
	defer stmt.Close()
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	_, err = stmt.Exec(user["Name"], text, time.Now().Unix())
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	ctx.Response(http.StatusCreated)
}
