package api

import (
	"chat/services"
	"database/sql"
	"errors"
	"fmt"
	"msf"
	"net/http"
	"regexp"
)

var (
	channelNameErr = errors.New("name of a channel must contain only lowercase english characters")
)

const (
	channelNameRegExp = "^[a-z]+$"

	createChannelFmt = `CREATE TABLE IF NOT EXISTS "%s" (user TEXT not null, text TEXT not null, time INTEGER not null);`
	removeChannelFmt = `DROP TABLE "%s"`

	getChannelsStmt = "SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%'"
)

type channel struct {
	Name string
}

func CreateChannel(ctx *msf.Context) {
	name := ctx.GetString("Name")

	match, _ := regexp.MatchString(channelNameRegExp, name)
	if !match {
		ctx.ResponseError(
			http.StatusBadRequest,
			channelNameErr)
		return
	}

	createChannelStmt := fmt.Sprintf(createChannelFmt, name)

	stmt, err := services.DB.Prepare(createChannelStmt)
	defer stmt.Close()
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	_, err = stmt.Exec(name)
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	ctx.Response(http.StatusCreated)
}

func GetChannels(ctx *msf.Context) {
	stmt, err := services.DB.Prepare(getChannelsStmt)
	defer stmt.Close()
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	var rows *sql.Rows
	rows, err = services.DB.Query(getChannelsStmt)
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	var channels []channel
	for rows.Next() {
		var c channel
		_ = rows.Scan(&c.Name)
		channels = append(channels, c)
	}

	ctx.ResponseJSON(http.StatusOK, channels)
}

func RemoveChannel(ctx *msf.Context) {
	name := ctx.GetString("Name")

	match, _ := regexp.MatchString(channelNameRegExp, name)
	if !match {
		ctx.ResponseError(
			http.StatusBadRequest,
			channelNameErr)
		return
	}

	removeChannelStmt := fmt.Sprintf(removeChannelFmt, name)

	stmt, err := services.DB.Prepare(removeChannelStmt)
	defer stmt.Close()
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	_, err = stmt.Exec(name)
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	ctx.Response(http.StatusOK)
}
