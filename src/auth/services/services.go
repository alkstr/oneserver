package services

import (
	"database/sql"
	"time"
)

var DB *sql.DB
var onlineCache map[string]time.Time

const initOnlineCacheStmt = "SELECT name FROM users"

func InitOnlineCache() error {
	onlineCache = map[string]time.Time{}
	stmt, err := DB.Prepare(initOnlineCacheStmt)
	defer stmt.Close()

	var rows *sql.Rows
	rows, err = DB.Query(initOnlineCacheStmt)
	if err != nil {
		return err
	}

	for rows.Next() {
		var name string
		_ = rows.Scan(&name)
		onlineCache[name] = time.Unix(0, 0)
	}

	return nil
}

func UpdateOnlineCache(name string) map[string]time.Time {
	onlineCache[name] = time.Now()
	return onlineCache
}
