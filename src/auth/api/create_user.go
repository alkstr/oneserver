package api

import (
	"auth/services"
	"math/rand"
	"msf"
	"net/http"
)

const createUserStmt = "INSERT INTO users (name, password, token) VALUES (?, ?, ?)"

func CreateUser(ctx *msf.Context) {
	name := ctx.GetString("Name")
	password := ctx.GetString("Password")

	stmt, err := services.DB.Prepare(createUserStmt)
	defer stmt.Close()
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	token := generateRandomString(16)
	// TODO: implement password hashing
	_, err = stmt.Exec(name, password, token)
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	ctx.Response(http.StatusCreated)
}

// TODO: implement more secure method
func generateRandomString(length int) string {
	charSet := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	randomString := make([]byte, length)
	for i := 0; i < length; i++ {
		randomString[i] = charSet[rand.Intn(len(charSet))]
	}
	return string(randomString)
}
