package api

import (
	"auth/services"
	"msf"
	"net/http"
	"time"
)

var onlineDuration, _ = time.ParseDuration("60s")

type online struct {
	Name   string
	Online bool
}

func GetOnline(ctx *msf.Context) {
	resp, err := ctx.RequestJSON("auth", "/get-user")
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	name := resp["Name"].(string)

	var onlineList []online
	now := time.Now()
	for n, t := range services.UpdateOnlineCache(name) {
		onlineList = append(onlineList, online{n, now.Sub(t) <= onlineDuration})
	}

	ctx.ResponseJSON(http.StatusOK, onlineList)
}
