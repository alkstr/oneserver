package api

import (
	"auth/services"
	"msf"
	"net/http"
)

const getUserStmt = "SELECT name FROM users WHERE token=$1"

func GetUser(ctx *msf.Context) {
	token := ctx.GetString("Token")

	stmt, err := services.DB.Prepare(getUserStmt)
	defer stmt.Close()
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	var name string
	err = services.DB.QueryRow(getUserStmt, token).Scan(&name)
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	ctx.ResponseJSON(http.StatusOK, struct{ Name string }{name})
}
