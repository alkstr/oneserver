package api

import (
	"auth/services"
	"msf"
	"net/http"
)

const getTokenStmt = "SELECT token FROM users WHERE name=$1 AND password=$2"

func GetToken(ctx *msf.Context) {
	name := ctx.GetString("Name")
	password := ctx.GetString("Password")

	stmt, err := services.DB.Prepare(getTokenStmt)
	defer stmt.Close()
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	var token string
	err = services.DB.QueryRow(getTokenStmt, name, password).Scan(&token)
	if err != nil {
		ctx.ResponseError(http.StatusInternalServerError, err)
		return
	}

	ctx.ResponseJSON(http.StatusOK, struct{ Token string }{Token: token})
}
