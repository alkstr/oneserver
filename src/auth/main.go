package main

import (
	"auth/api"
	"auth/services"
	"log"
	_ "modernc.org/sqlite"
	"msf"
)

var addrMap = map[string]string{
	"gateway": "http://localhost:8100",
	"auth":    "http://localhost:8120",
	"chat":    "http://localhost:8121",
}

func main() {
	ms := msf.New()

	services.DB = ms.InitDB("sqlite", "auth.sqlite")
	defer services.DB.Close()
	err := services.InitOnlineCache()
	if err != nil {
		log.Fatal(err)
	}

	ms.InitAddrMap(addrMap)

	ms.POST("/get-user", api.GetUser)
	ms.POST("/create-user", api.CreateUser)
	ms.POST("/get-token", api.GetToken)
	ms.POST("/get-online", api.GetOnline)

	ms.Start(":8120")
}
